Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: zmat
Upstream-Contact: fangqq@gmail.com
Source: https://github.com/fangq/zmat
Files-Excluded: private/* octave/*
Comment: Exclude precompiled binaries to ease packaging

Files: *
Copyright: 2019-2020 Qianqian Fang
License: GPL-3+

Files: src/easylzma/*
Copyright: 2009 Lloyd Hilaiel (lloyd)
License: public-domain

Files: src/easylzma/pavlov/*
Copyright: 2008 Igor Pavlov
License: public-domain

Files: debian/*
Copyright: 2020 Qianqian Fang
           2020 Rafael Laboissière <rafael@debian.org>
	   2013–2014 Ben Finney <ben+debian@benfinney.id.au>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: public-domain
 The source code was released in the Public Domain by its author.
